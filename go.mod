module gitlab.com/ribtoks/gitlab-tdg

go 1.18

require (
	github.com/xanzy/go-gitlab v0.115.0
	gitlab.com/ribtoks/tdg v0.0.6
)

require (
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.7 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/zieckey/goini v0.0.0-20180118150432-0da17d361d26 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/oauth2 v0.6.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.29.1 // indirect
)
