package main

import (
	"context"
	"fmt"
	"log"
	"math"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/ribtoks/tdg/pkg/tdglib"
)

const (
	defaultMinWords      = 3
	defaultMinChars      = 30
	defaultAddLimit      = 0
	defaultCloseLimit    = 0
	defaultIssuesPerPage = 200
	defaultConcurrency   = 128
	contextLinesUp       = 3
	contextLinesDown     = 7
	defaultLabel         = "todo comment"
	minEstimate          = 0.01
	hourMinutes          = 60
	labelBranchPrefix    = "branch::"
	labelTypePrefix      = "type::"
	labelAreaPrefix      = "area::"
	labelLangPrefix      = "lang::"
	stateClosed          = "closed"
	eventClose           = "close"
)

func sourceRoot(root string) string {
	// currently workaround for path are removed
	return root
}

type env struct {
	root              string
	repoPath          string
	label             string
	token             string
	pid               string
	sha               string
	ref               string
	branch            string
	includeRE         string
	excludeRE         string
	projectColumnID   int64
	minWords          int
	minChars          int
	addLimit          int
	closeLimit        int
	concurrency       int
	closeOnSameBranch bool
	extendedLabels    bool
	dryRun            bool
	commentIssue      bool
	assignFromBlame   bool
}

type service struct {
	ctx    context.Context
	client *gitlab.Client
	env    *env
	wg     sync.WaitGroup
}

func (e *env) sourceRoot() string {
	return sourceRoot(e.root)
}

func flagToBool(s string) bool {
	s = strings.ToLower(s)
	return s == "1" || s == "true" || s == "y" || s == "yes"
}

func environment() *env {
	// docs https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
	ref := os.Getenv("TDG_REF")
	e := &env{
		ref:               ref,
		repoPath:          os.Getenv("TDG_REPO"),
		branch:            branch(ref),
		pid:               os.Getenv("TDG_PROJECT_ID"),
		sha:               os.Getenv("TDG_SHA"),
		root:              os.Getenv("TDG_ROOT"),
		label:             os.Getenv("TDG_LABEL"),
		token:             os.Getenv("TDG_TOKEN"),
		includeRE:         os.Getenv("TDG_INCLUDE_PATTERN"),
		excludeRE:         os.Getenv("TDG_EXCLUDE_PATTERN"),
		dryRun:            flagToBool(os.Getenv("TDG_DRY_RUN")),
		extendedLabels:    flagToBool(os.Getenv("TDG_EXTENDED_LABELS")),
		closeOnSameBranch: flagToBool(os.Getenv("TDG_CLOSE_ON_SAME_BRANCH")),
		commentIssue:      flagToBool(os.Getenv("TDG_COMMENT_ON_ISSUES")),
		assignFromBlame:   flagToBool(os.Getenv("TDG_ASSIGN_FROM_BLAME")),
	}

	var err error

	e.minWords, err = strconv.Atoi(os.Getenv("TDG_MIN_WORDS"))
	if err != nil {
		e.minWords = defaultMinWords
	}

	e.minChars, err = strconv.Atoi(os.Getenv("TDG_MIN_CHARACTERS"))
	if err != nil {
		e.minChars = defaultMinChars
	}

	e.addLimit, err = strconv.Atoi(os.Getenv("TDG_ADD_LIMIT"))
	if err != nil {
		e.addLimit = defaultAddLimit
	}

	e.closeLimit, err = strconv.Atoi(os.Getenv("TDG_CLOSE_LIMIT"))
	if err != nil {
		e.closeLimit = defaultCloseLimit
	}

	e.concurrency, err = strconv.Atoi(os.Getenv("TDG_CONCURRENCY"))
	if err != nil {
		e.concurrency = defaultConcurrency
	}

	e.projectColumnID, err = strconv.ParseInt(os.Getenv("TDG_PROJECT_COLUMN_ID"), 10, 64)
	if err != nil {
		e.projectColumnID = -1
	}

	if len(e.label) == 0 {
		e.label = defaultLabel
	}

	return e
}

func (e *env) debugPrint() {
	log.Printf("Repo: %v", e.repoPath)
	log.Printf("Ref: %v", e.ref)
	log.Printf("Sha: %v", e.sha)
	log.Printf("Root: %v", e.root)
	log.Printf("Label: %v", e.label)
	log.Printf("Project ID: %v", e.pid)
	log.Printf("Column ID: %v", e.projectColumnID)
	log.Printf("Extended labels: %v", e.extendedLabels)
	log.Printf("Min words: %v", e.minWords)
	log.Printf("Min chars: %v", e.minChars)
	log.Printf("Add limit: %v", e.addLimit)
	log.Printf("Close limit: %v", e.closeLimit)
	log.Printf("Close on same branch: %v", e.closeOnSameBranch)
	log.Printf("Dry run: %v", e.dryRun)
	log.Printf("Token length: %v", len(e.token))
	log.Printf("Assign from blame: %v", e.assignFromBlame)
}

func branch(ref string) string {
	parts := strings.Split(ref, "/")
	skip := map[string]bool{"refs": true, "tags": true, "heads": true, "remotes": true}
	i := 0

	for i = 0; i < len(parts); i++ {
		if _, ok := skip[parts[i]]; !ok {
			break
		}
	}

	return strings.Join(parts[i:], "/")
}

func (s *service) fetchGitlabIssues() ([]*gitlab.Issue, error) {
	var allIssues []*gitlab.Issue

	state := "all"

	labels := []string{s.env.label}
	glLabels := gitlab.LabelOptions(labels)

	opt := &gitlab.ListProjectIssuesOptions{
		Labels:      &glLabels,
		State:       &state,
		ListOptions: gitlab.ListOptions{PerPage: defaultIssuesPerPage},
	}

	for {
		issues, resp, err := s.client.Issues.ListProjectIssues(s.env.pid, opt)
		if err != nil {
			return nil, err
		}

		allIssues = append(allIssues, issues...)

		if resp.NextPage == 0 {
			break
		}

		opt.Page = resp.NextPage
	}
	log.Printf("Fetched gitlab todo issues. count=%v label=%v", len(allIssues), s.env.label)

	return allIssues, nil
}

func escapePath(path string) string {
	parts := strings.Split(path, "/")
	for i := range parts {
		parts[i] = url.PathEscape(parts[i])
	}

	return strings.Join(parts, "/")
}

func (s *service) createFileLink(c *tdglib.ToDoComment) string {
	start := c.Line - contextLinesUp
	if start < 0 {
		start = 0
	}

	end := c.Line + contextLinesDown
	filepath := c.File
	safeFilepath := escapePath(filepath)

	// https://gitlab.com/{repo}/blob/{sha}/{file}#L{startLines}-L{endLine}
	return fmt.Sprintf("https://gitlab.com/%s/blob/%s/%s#L%v-%v",
		s.env.repoPath, s.env.sha, safeFilepath, start, end)
}

func (s *service) labels(c *tdglib.ToDoComment) []string {
	labels := []string{s.env.label}
	if s.env.extendedLabels {
		labels = append(labels, labelBranchPrefix+s.env.branch)
		labels = append(labels, labelTypePrefix+strings.ToLower(c.Type))

		if len(c.Category) > 0 {
			labels = append(labels, labelAreaPrefix+c.Category)
		}

		if extension := filepath.Ext(c.File); len(extension) > 0 {
			labels = append(labels, labelLangPrefix+strings.ToLower(extension))
		}

		if c.Estimate > minEstimate {
			minutes := math.Round(c.Estimate * hourMinutes)
			estimate := ""

			if minutes >= hourMinutes {
				// -1 means use the smallest number of digits
				estimate = strconv.FormatFloat(c.Estimate, 'f', -1, 32) + "h"
			} else {
				estimate = fmt.Sprintf("%vm", minutes)
			}

			labels = append(labels, fmt.Sprintf("estimate: %v", estimate))
		}
	}

	return labels
}

// func (s *service) createProjectCard(issue *gitlab.Issue) {
// 	opts := &gitlab.ProjectCardOptions{
// 		ContentType: "Issue",
// 		ContentID:   issue.GetID(),
// 	}
// 	card, _, err := s.client.Projects.CreateProjectCard(s.ctx, s.env.projectColumnID, opts)
//
// 	if err != nil {
// 		log.Printf("Failed to create a project card. err=%v", err)
// 		return
// 	}
//
// 	log.Printf("Created a project card. issue=%v card=%v", issue.GetID(), card.GetID())
// }

func (s *service) openNewIssues(issueMap map[string]*gitlab.Issue, comments []*tdglib.ToDoComment) {
	defer s.wg.Done()
	count := 0

	for _, c := range comments {
		_, ok := issueMap[c.Title]
		if !ok {
			body := c.Body + "\n\n"
			if c.Issue > 0 {
				body += fmt.Sprintf("Parent issue: #%v\n\n", c.Issue)
			}

			if len(c.Author) > 0 {
				body += fmt.Sprintf("Author: @%s\n\n", c.Author)
			}

			body += fmt.Sprintf("Line: %v\n\n%s", c.Line, s.createFileLink(c))

			log.Printf("About to create an issue. title=%v body=%v", c.Title, body)

			if s.env.dryRun {
				log.Printf("Dry run mode.")
				continue
			}

			labels := gitlab.LabelOptions(s.labels(c))
			opts := &gitlab.CreateIssueOptions{
				Title:       &c.Title,
				Description: &body,
				Labels:      &labels,
			}

			issue, _, err := s.client.Issues.CreateIssue(s.env.repoPath, opts)
			if err != nil {
				log.Printf("Error while creating an issue. err=%v", err)
				continue
			}

			log.Printf("Created an issue. title=%v issue=%v", c.Title, issue.ID)

			// if s.env.projectColumnID != -1 {
			// 	s.createProjectCard(issue)
			// }

			count++
			if s.env.addLimit > 0 && count >= s.env.addLimit {
				log.Printf("Exceeded limit of issues to create. limit=%v", s.env.addLimit)
				break
			}
		}
	}

	log.Printf("Created new issues. count=%v", count)
}

func (s *service) canCloseIssue(issue *gitlab.Issue) bool {
	if !s.env.closeOnSameBranch {
		return true
	}

	labels := issue.Labels
	anyBranch := false

	log.Printf("Retrieved issue labels. issue=%v labels=%v", issue.ID, len(labels))

	for _, l := range labels {
		name := l
		if strings.HasPrefix(name, labelBranchPrefix) {
			anyBranch = true
			branch := strings.TrimPrefix(name, labelBranchPrefix)

			if branch == s.env.branch {
				return true
			}
		}
	}

	log.Printf("Checking issue labels. issue=%v any_branch=%v", issue.ID, anyBranch)

	// if the issues does not have a branch tag, assume we can close it
	return !anyBranch
}

func (s *service) commentIssue(body string, i *gitlab.Issue) {
	comment := &gitlab.CreateIssueNoteOptions{
		Body: &body,
	}
	_, _, err := s.client.Notes.CreateIssueNote(s.env.pid, i.IID, comment)
	if err != nil {
		log.Printf("Error while adding a comment. issue=%v err=%v", i.IID, err)
		return
	}

	log.Printf("Added a comment to the issue. issue=%v", i.IID)
}

func (s *service) closeMissingIssues(issueMap map[string]*gitlab.Issue, comments []*tdglib.ToDoComment) {
	defer s.wg.Done()

	count := 0
	commentsMap := make(map[string]*tdglib.ToDoComment)

	for _, c := range comments {
		commentsMap[c.Title] = c
	}

	for _, i := range issueMap {
		if _, ok := commentsMap[i.Title]; ok {
			continue
		}

		log.Printf("About to close an issue. issue=%v title=%v", i.ID, i.Title)

		if s.env.dryRun {
			log.Printf("Dry run mode")
			continue
		}

		if i.State == stateClosed {
			log.Printf("Issue is already closed. issue=%v", i.ID)
			continue
		}

		canClose := s.canCloseIssue(i)
		if !canClose {
			log.Printf("Cannot close the issue. issue=%v", i.ID)
			continue
		}

		if s.env.commentIssue {
			s.commentIssue(fmt.Sprintf("Closed in commit %v", s.env.sha), i)
		}

		stateEvent := eventClose
		opts := &gitlab.UpdateIssueOptions{
			StateEvent: &stateEvent,
		}
		_, _, err := s.client.Issues.UpdateIssue(s.env.pid, i.IID, opts)

		if err != nil {
			log.Printf("Error while closing an issue. issue=%v err=%v", i.ID, err)
			continue
		}

		log.Printf("Closed an issue. issue=%v", i.ID)

		count++
		if s.env.closeLimit > 0 && count >= s.env.closeLimit {
			log.Printf("Exceeded limit of issues to close. limit=%v", s.env.closeLimit)
			break
		}
	}

	log.Printf("Closed issues. count=%v", count)
}

// TODO: this is a test issue for this project
// estimate=1h
// we need to test something so this might be a good candidate for it
func main() {
	log.SetOutput(os.Stdout)

	env := environment()
	ctx := context.Background()

	git, err := gitlab.NewClient(env.token)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	svc := &service{
		ctx:    ctx,
		client: git,
		env:    env,
	}

	env.debugPrint()

	issues, err := svc.fetchGitlabIssues()
	if err != nil {
		log.Panic(err)
	}

	includePatterns := make([]string, 0)
	if len(env.includeRE) > 0 {
		includePatterns = append(includePatterns, env.includeRE)
	}

	excludePatterns := make([]string, 0)
	if len(env.excludeRE) > 0 {
		excludePatterns = append(excludePatterns, env.excludeRE)
	}

	td := tdglib.NewToDoGenerator(env.sourceRoot(),
		includePatterns,
		excludePatterns,
		env.assignFromBlame,
		env.minWords,
		env.minChars,
		env.concurrency)

	comments, err := td.Generate()
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Extracted TODO comments. count=%v", len(comments))

	issueMap := make(map[string]*gitlab.Issue)
	for _, i := range issues {
		issueMap[i.Title] = i
	}

	svc.wg.Add(1)
	go svc.closeMissingIssues(issueMap, comments)

	svc.wg.Add(1)
	go svc.openNewIssues(issueMap, comments)

	log.Printf("Waiting for issues management to finish")
	svc.wg.Wait()
}
