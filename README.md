# Gitlab TDG

Integrate tdg and GitLab to create and close issues based on TODO/FIXME/HACK comments in the source code. Source code is parsed using [tdg](https://gitlab.com/ribtoks/tdg) which supports comments for almost all existing languages.

When a new todo comment is added, a new issue is created. When this comment is removed on the branch it was added, the corresponding issue is closed. Each issue is added with a special label so you can build more automation on top of it.

This application is a port of [GitHub tdg action](https://github.com/ribtoks/tdg-github-action/) that does the same for GitHub. Given that GitLab CI is quite different from GitHub Actions, this is a separate application that you need to call as part of your CI pipeline, rather than being a pipeline itself.

## Screenshot

![example of an issue](gitlab-issues.png)

## Usage

In order to use this app, you need to get the binary into your CI pipeline. You can do it using `go get` command or you can prebuild it and download ready binary using `curl` or `wget`. `gitlab-tdg` reads certain environment variables (inherited from [github-tdg-action](https://github.com/ribtoks/tdg-github-action/)) instead of command-line parameters.

Also you need to create a PAT (personal access token) in your GitLab user's settings page -> Access Tokens with access to the API and then in repository settings CI/CD Variables section add it as a variable (don't forget to protect and mask it).

### Basic example

Example in the `.gitlab-ci.yml`:

```
todo:
  image: golang:1.21
  stage: issues
  cache: []
  variables: 
    TDG_REPO: "${CI_PROJECT_PATH}"
    TDG_REF: "${CI_COMMIT_REF_NAME}"
    TDG_SHA: "${CI_COMMIT_SHA}"
    TDG_ROOT: "${CI_PROJECT_DIR}"
    TDG_TOKEN: "${TDG_PAT}"
    TDG_PROJECT_ID: "${CI_PROJECT_ID}"
  before_script:
    - go install gitlab.com/ribtoks/gitlab-tdg/cmd/gitlab-tdg@latest
  script:
    - gitlab-tdg
  allow_failure: true
```

Out of these varialbes you only need to create a PAT with access to the API and provide it through CI/CD variables (`TDG_PAT`) in the example above.

### All parameters

Variable | Meaning
--- | ---
`TDG_REPO` | Repository name in the format of `owner/repo` (required)
`TDG_REF` | Git ref: branch or pull request (required)
`TDG_SHA` | SHA-1 value of the commit (required)
`TDG_ROOT` | Source code root (defaults to `.`)
`TDG_TOKEN` | GitLab token used to create or close issues (required) 
`TDG_PROJECT_ID` | GitLab project ID (required)
`TDG_EXTENDED_LABELS`  | Add additional labels to mark branch, code language, issue type and estimate
`TDG_CLOSE_ON_SAME_BRANCH`  | Close issues only if they are missing from the same branch as they were created on (by default)
`TDG_INCLUDE_PATTERN`  | Regex to include source code files (includes all by default)
`TDG_EXCLUDE_PATTERN`  | Regex to exclude source code files (excludes none by default)
`TDG_MIN_WORDS`  | Minimum number of words in the comment to become an issue (defaults to `3`)
`TDG_MIN_CHARACTERS`  | Minimum number of characters in the comment to become an issue (defaults to `30`)
`TDG_DRY_RUN`  | Do not open or close real issues (used for debugging)
`TDG_ADD_LIMIT`  | Upper cap on the number of issues to create (defaults to `0` - unlimited)
`TDG_CLOSE_LIMIT`  | Upper cap on the number of issues to close (defaults to `0` - unlimited)
`TDG_COMMENT_ON_ISSUES` | Comment on issues with closing reason (defauls to `0` - do not comment)
`TDG_CONCURRENCY` | How many files to process in parallel (defaults to `128`)
`TDG_ASSIGN_FROM_BLAME` | Get the author of the comment via git API from the commit hash of the comment and assign to the issue created (defaults to `0` - do not use)

Flag values like `CLOSE_ON_SAME_BRANCH` or `DRY_RUN` use values `1`/`true`/`y` as ON switch. In case you are disabling `EXTENDED_LABELS`, then `CLOSE_ON_SAME_BRANCH` logic will be broken since there will be no knowledge on which branch the issue was created (for new issues), effectively making it disabled.

### TODO comments

Comments are parsed using [tdg](https://gitlab.com/ribtoks/tdg). Supported comments: `//`, `#`, `%`, `;`, `*`.

Example of the comment (everything but the first line is optional):

    // TODO: This is title of the issue to create
    // category=SomeCategory issue=123 estimate=30m author=alias
    // This is a multiline description of the issue
    // that will be in the "Body" property of the comment

Note that second line has some optional "extensions" added as metadata to the issue by [tdg](https://github.com/ribtoks/tdg). Some are turned into labels.
