build:
	export GOFLAGS="-mod=vendor"
	go build -o bin/gitlab-tdg cmd/gitlab-tdg/*

vendors:
	go mod tidy
	go mod vendor
